import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

let Register = require('./pages/Register.vue').default;
let Users = require('./pages/Users.vue').default;

const router = new Router({
    mode: 'history',
    routes: [
        {path: '/', name: 'home', component: Register},
        {path: '/Users', name: 'users', component: Users},
    ]
});

export default router
