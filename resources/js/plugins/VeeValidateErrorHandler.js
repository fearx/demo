import Vue from 'vue'
Vue.prototype.$setErrorsFromResponse = function(errorResponse) {
    // only allow this function to be run if the validator exists
    if (!this.hasOwnProperty('$validator')) {
        return;
    }
    // clear errors
    this.$validator.errors.clear();
    // check if errors exist
    if (!errorResponse.hasOwnProperty('errors')) {
        if(!errorResponse.message){
            return;
        }else{
            return this.$validator.errors.add({
                field: 'message',
                msg: errorResponse.message
            });
        }

    }

    let errorFields = Object.keys(errorResponse.errors);
    // insert laravel errors
    errorFields.map(field => {
        let errorString = errorResponse.errors[field].join(', ');
        this.$validator.errors.add({
            field: field,
            msg: errorString
        });
    });
}
