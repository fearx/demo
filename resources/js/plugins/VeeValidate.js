import Vue from "vue";
import VeeValidate from 'vee-validate';

import sk from 'vee-validate/dist/locale/sk';

const veeConfig = {
    errorBagName: "errors", 
    fieldsBagName: "fields",
    delay: 10,
    dictionary: {
        sk
    },
    strict: true,
    classes: false,
    classNames: {
        touched: "touched", // the control has been blurred
        untouched: "untouched", // the control hasn't been blurred
        valid: "valid", // model is valid
        invalid: "invalid", // model is invalid
        pristine: "pristine", // control has not been interacted with
        dirty: "dirty" // control has been interacted with
    },
    events: "input|blur",
    inject: true,
    validity: false,
    aria: true
};
Vue.use(VeeValidate, veeConfig);
