import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import storeData from "../store/Index"

const store = new Vuex.Store(
   storeData
)

export default store