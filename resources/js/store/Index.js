import axios from 'axios'
export default {
	state: {
       users: []
	},

	getters:{
        users: state => state.users,
	},
    // mutations
    mutations: {
        SET_USERS (state, users) {
            state.users = users
        },
    },
	actions: {
        async setUsers({ commit }, form) {
            try {
                const response = await axios.get('/user')
                commit('SET_USERS', response.data)
            } catch (e) {
                return false;
            }
        },
        register({ commit }, form) {
            return new Promise((resolve, reject) => {
                axios.post('/user', form).then(response => {
                    resolve(response.data)
                }).catch(error => {
                    reject(error)
                })
            })
        },
	},

}