import Vue from 'vue'

import router from './routes'
import store from './plugins/Vuex'
import "./plugins/VeeValidate"
import "./plugins/VeeValidateErrorHandler"

import App from './components/App'




import './components'
import Form from './utilities/Form';
global.Form = Form;


/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    components: {
        App
    },
    router,
    render: h => h(App)
})

