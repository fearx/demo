<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;


class UserController extends Controller
{

    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * @param UserRequest $request
     * @return mixed
     */
    public function store(UserRequest $request)
    {

        return User::create($request->only(['name', 'email', 'sex', 'date_of_britday']));
    }
}
